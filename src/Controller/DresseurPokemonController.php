<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * DresseurPokemon Controller
 *
 * @property \App\Model\Table\DresseurPokemonTable $DresseurPokemon
 *
 * @method \App\Model\Entity\DresseurPokemon[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DresseurPokemonController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Dresseurs', 'Pokemon']
        ];
        $dresseurPokemon = $this->paginate($this->DresseurPokemon);

        $this->set(compact('dresseurPokemon'));
    }

    /**
     * View method
     *
     * @param string|null $id Dresseur Pokemon id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $dresseurPokemon = $this->DresseurPokemon->get($id, [
            'contain' => ['Dresseurs', 'Pokemon']
        ]);

        $this->set('dresseurPokemon', $dresseurPokemon);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $dresseurPokemon = $this->DresseurPokemon->newEntity();
        if ($this->request->is('post')) {
            $dresseurPokemon = $this->DresseurPokemon->patchEntity($dresseurPokemon, $this->request->getData());
            if ($this->DresseurPokemon->save($dresseurPokemon)) {
                $this->Flash->success(__('The dresseur pokemon has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The dresseur pokemon could not be saved. Please, try again.'));
        }
        $dresseurs = $this->DresseurPokemon->Dresseurs->find('list', ['limit' => 200]);
        $pokemon = $this->DresseurPokemon->Pokemon->find('list', ['limit' => 200]);
        $this->set(compact('dresseurPokemon', 'dresseurs', 'pokemon'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Dresseur Pokemon id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $dresseurPokemon = $this->DresseurPokemon->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $dresseurPokemon = $this->DresseurPokemon->patchEntity($dresseurPokemon, $this->request->getData());
            if ($this->DresseurPokemon->save($dresseurPokemon)) {
                $this->Flash->success(__('The dresseur pokemon has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The dresseur pokemon could not be saved. Please, try again.'));
        }
        $dresseurs = $this->DresseurPokemon->Dresseurs->find('list', ['limit' => 200]);
        $pokemon = $this->DresseurPokemon->Pokemon->find('list', ['limit' => 200]);
        $this->set(compact('dresseurPokemon', 'dresseurs', 'pokemon'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Dresseur Pokemon id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $dresseurPokemon = $this->DresseurPokemon->get($id);
        if ($this->DresseurPokemon->delete($dresseurPokemon)) {
            $this->Flash->success(__('The dresseur pokemon has been deleted.'));
        } else {
            $this->Flash->error(__('The dresseur pokemon could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
