<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DresseurPokemon Model
 *
 * @property \App\Model\Table\DresseursTable&\Cake\ORM\Association\BelongsTo $Dresseurs
 * @property \App\Model\Table\PokemonTable&\Cake\ORM\Association\BelongsTo $Pokemon
 *
 * @method \App\Model\Entity\DresseurPokemon get($primaryKey, $options = [])
 * @method \App\Model\Entity\DresseurPokemon newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DresseurPokemon[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DresseurPokemon|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DresseurPokemon saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DresseurPokemon patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DresseurPokemon[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DresseurPokemon findOrCreate($search, callable $callback = null, $options = [])
 */
class DresseurPokemonTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('dresseur_pokemon');
        $this->setDisplayField('ID');
        $this->setPrimaryKey('ID');

        $this->belongsTo('Dresseurs', [
            'foreignKey' => 'dresseur_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Pokemon', [
            'foreignKey' => 'pokemon_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('ID')
            ->allowEmptyString('ID', null, 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['dresseur_id'], 'Dresseurs'));
        $rules->add($rules->existsIn(['pokemon_id'], 'Pokemon'));

        return $rules;
    }
}
