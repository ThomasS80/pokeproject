<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Dresseurs Model
 *
 * @method \App\Model\Entity\Dresseur get($primaryKey, $options = [])
 * @method \App\Model\Entity\Dresseur newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Dresseur[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Dresseur|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Dresseur saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Dresseur patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Dresseur[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Dresseur findOrCreate($search, callable $callback = null, $options = [])
 */
class DresseursTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('dresseurs');
        $this->setDisplayField('dresseur_id');
        $this->setPrimaryKey('dresseur_id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->scalar('Nom')
            ->requirePresence('Nom', 'create')
            ->notEmptyString('Nom');

        $validator
            ->scalar('Prenom')
            ->requirePresence('Prenom', 'create')
            ->notEmptyString('Prenom');

        $validator
            ->integer('dresseur_id')
            ->allowEmptyString('dresseur_id', null, 'create');

        return $validator;
    }
}
