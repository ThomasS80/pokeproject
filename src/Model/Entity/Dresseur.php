<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Dresseur Entity
 *
 * @property string $Nom
 * @property string $Prenom
 * @property int $dresseur_id
 */
class Dresseur extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'Nom' => true,
        'Prenom' => true
    ];
}
