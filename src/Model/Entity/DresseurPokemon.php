<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DresseurPokemon Entity
 *
 * @property int $dresseur_id
 * @property int $pokemon_id
 * @property int $ID
 *
 * @property \App\Model\Entity\Dresseur $dresseur
 * @property \App\Model\Entity\Pokemon $pokemon
 */
class DresseurPokemon extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'dresseur_id' => true,
        'pokemon_id' => true,
        'dresseur' => true,
        'pokemon' => true
    ];
}
