<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Dresseur $dresseur
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Dresseur'), ['action' => 'edit', $dresseur->dresseur_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Dresseur'), ['action' => 'delete', $dresseur->dresseur_id], ['confirm' => __('Are you sure you want to delete # {0}?', $dresseur->dresseur_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Dresseurs'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dresseur'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="dresseurs view large-9 medium-8 columns content">
    <h3><?= h($dresseur->dresseur_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Dresseur Id') ?></th>
            <td><?= $this->Number->format($dresseur->dresseur_id) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Nom') ?></h4>
        <?= $this->Text->autoParagraph(h($dresseur->Nom)); ?>
    </div>
    <div class="row">
        <h4><?= __('Prenom') ?></h4>
        <?= $this->Text->autoParagraph(h($dresseur->Prenom)); ?>
    </div>
</div>
