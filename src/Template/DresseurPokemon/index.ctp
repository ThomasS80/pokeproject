<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DresseurPokemon[]|\Cake\Collection\CollectionInterface $dresseurPokemon
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Dresseur Pokemon'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dresseurs'), ['controller' => 'Dresseurs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dresseur'), ['controller' => 'Dresseurs', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pokemon'), ['controller' => 'Pokemon', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pokemon'), ['controller' => 'Pokemon', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="dresseurPokemon index large-9 medium-8 columns content">
    <h3><?= __('Dresseur Pokemon') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('dresseur_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('pokemon_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('ID') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($dresseurPokemon as $dresseurPokemon): ?>
            <tr>
                <td><?= $dresseurPokemon->has('dresseur') ? $this->Html->link($dresseurPokemon->dresseur->dresseur_id, ['controller' => 'Dresseurs', 'action' => 'view', $dresseurPokemon->dresseur->dresseur_id]) : '' ?></td>
                <td><?= $dresseurPokemon->has('pokemon') ? $this->Html->link($dresseurPokemon->pokemon->ID, ['controller' => 'Pokemon', 'action' => 'view', $dresseurPokemon->pokemon->ID]) : '' ?></td>
                <td><?= $this->Number->format($dresseurPokemon->ID) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $dresseurPokemon->ID]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $dresseurPokemon->ID]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $dresseurPokemon->ID], ['confirm' => __('Are you sure you want to delete # {0}?', $dresseurPokemon->ID)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
