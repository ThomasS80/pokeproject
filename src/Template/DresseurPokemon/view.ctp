<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DresseurPokemon $dresseurPokemon
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Dresseur Pokemon'), ['action' => 'edit', $dresseurPokemon->ID]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Dresseur Pokemon'), ['action' => 'delete', $dresseurPokemon->ID], ['confirm' => __('Are you sure you want to delete # {0}?', $dresseurPokemon->ID)]) ?> </li>
        <li><?= $this->Html->link(__('List Dresseur Pokemon'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dresseur Pokemon'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dresseurs'), ['controller' => 'Dresseurs', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dresseur'), ['controller' => 'Dresseurs', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pokemon'), ['controller' => 'Pokemon', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pokemon'), ['controller' => 'Pokemon', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="dresseurPokemon view large-9 medium-8 columns content">
    <h3><?= h($dresseurPokemon->ID) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Dresseur') ?></th>
            <td><?= $dresseurPokemon->has('dresseur') ? $this->Html->link($dresseurPokemon->dresseur->dresseur_id, ['controller' => 'Dresseurs', 'action' => 'view', $dresseurPokemon->dresseur->dresseur_id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pokemon') ?></th>
            <td><?= $dresseurPokemon->has('pokemon') ? $this->Html->link($dresseurPokemon->pokemon->ID, ['controller' => 'Pokemon', 'action' => 'view', $dresseurPokemon->pokemon->ID]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('ID') ?></th>
            <td><?= $this->Number->format($dresseurPokemon->ID) ?></td>
        </tr>
    </table>
</div>
