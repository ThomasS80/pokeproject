<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Pokemon $pokemon
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Pokemon'), ['action' => 'edit', $pokemon->ID]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Pokemon'), ['action' => 'delete', $pokemon->ID], ['confirm' => __('Are you sure you want to delete # {0}?', $pokemon->ID)]) ?> </li>
        <li><?= $this->Html->link(__('List Pokemon'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pokemon'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dresseur Pokemon'), ['controller' => 'DresseurPokemon', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dresseur Pokemon'), ['controller' => 'DresseurPokemon', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="pokemon view large-9 medium-8 columns content">
    <h3><?= h($pokemon->ID) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('ID') ?></th>
            <td><?= $this->Number->format($pokemon->ID) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Nom') ?></h4>
        <?= $this->Text->autoParagraph(h($pokemon->Nom)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Dresseur Pokemon') ?></h4>
        <?php if (!empty($pokemon->dresseur_pokemon)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Dresseur Id') ?></th>
                <th scope="col"><?= __('Pokemon Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($pokemon->dresseur_pokemon as $dresseurPokemon): ?>
            <tr>
                <td><?= h($dresseurPokemon->dresseur_id) ?></td>
                <td><?= h($dresseurPokemon->pokemon_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'DresseurPokemon', 'action' => 'view', $dresseurPokemon->]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'DresseurPokemon', 'action' => 'edit', $dresseurPokemon->]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'DresseurPokemon', 'action' => 'delete', $dresseurPokemon->], ['confirm' => __('Are you sure you want to delete # {0}?', $dresseurPokemon->)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
