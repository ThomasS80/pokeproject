<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Pokemon $pokemon
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $pokemon->ID],
                ['confirm' => __('Are you sure you want to delete # {0}?', $pokemon->ID)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Pokemon'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Dresseur Pokemon'), ['controller' => 'DresseurPokemon', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dresseur Pokemon'), ['controller' => 'DresseurPokemon', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pokemon form large-9 medium-8 columns content">
    <?= $this->Form->create($pokemon) ?>
    <fieldset>
        <legend><?= __('Edit Pokemon') ?></legend>
        <?php
            echo $this->Form->control('Nom');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
